<?php

/*
 * Arthrology (module) for Elxis CMS 2008.x/2009.x
 *
 * @version		1.0
 * @package		Arthrology
 * @author		Apostolos Koutsoulelos <akoutsoulelos@yahoo.gr>
 * @copyright	Copyright (C) 2009 Apostolos Koutsoulelos. All rights reserved.
 * @license		http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @link		http://www.elxis-downloads.com/com_downloads/miscellaneous/167.html
 * 
 */

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

// Get globals
global $lang;

// Includes
if ( file_exists($mosConfig_absolute_path.'/modules/mod_arthrology/'.$lang.'.php') ) {
	require_once( $mosConfig_absolute_path.'/modules/mod_arthrology/'.$lang.'.php' );
} else {
	require_once( $mosConfig_absolute_path.'/modules/mod_eventcalendar/language/english.php' );
}

// Check if module is already declared
if (!function_exists('mod_arthrology')) {

	function mod_arthrology() {
		global $Itemid, $database, $my, $lang, $mainframe;
		
		$menuitem = $Itemid;
		if ($mainframe->getCfg('sef') != 2) {
			$access = !$mainframe->getCfg('shownoauth');
			$query = "SELECT id FROM #__menu WHERE link='index.php?option=com_arthrology' AND published='1'"
				."\n AND ((language IS NULL) OR (language LIKE '%".$lang."%'))"
				.($access ? "\n AND access IN (".$my->allowed.")" : '');
			$database->setQuery($query, '#__', 1, 0);
			$menuitem = intval($database->loadResult());
			if (!$menuitem) { $menuitem = $Itemid; }
		}
		
		$seoAction = sefRelToAbs('index.php?option=com_arthrology&task=results&Itemid='.$menuitem, "arthrology/results.html");
		?>
		
		<div class="searchbox">
			<form action="<?php echo $seoAction ?>" method="post" name="hmtlForm">

				<?php echo CX_ARTH_SRCH_ENTER; ?> <br/><br/>
				
				<table><tr>
					<td>
						<b><?php echo CX_ARTH_SRCH_MAG; ?>:</b>
					</td><td>
						<?php echo mosAdminMenus::ComponentCategory('catid','com_arthrology'); ?>
					</td>
				</tr><tr>
					<td>
						<b><?php echo CX_ARTH_SRCH_KEYW; ?>:</b>
					</td><td>
						<input type="text" name="keyword" value="" class="inputbox" size="40" />
					</td>				
				</tr></table>

				<input type="hidden" name="option" value="com_arthrology" />
				<input type="hidden" name="Itemid" value="<?php echo $Itemid; ?>" />
				
				<p align="center">
					<input type="submit" value="<?php echo _E_SEARCH; ?>" title="<?php echo _E_SEARCH; ?>" class="button" />
				</p>	
			</form>
		</div>
		<?php
	}
	
}

// Get module parameters

// Start module
mod_arthrology();

?>
