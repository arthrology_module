<?php 

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define('_ISO','charset=UTF-8');

define('CX_ARTH_SRCH_ENTER', 'Εισάγετε μία λέξη κλειδί προς αναζήτηση (ελάχιστο μήκος 3 χαρακτήρες). Αφήστε κενό για σύνολο άρθρων επιλεγμένου περιοδικού.');
define('CX_ARTH_SRCH_MAG', 'Περιοδικό');
define('CX_ARTH_SRCH_KEYW', 'Λέξη Κλειδί');
?>
