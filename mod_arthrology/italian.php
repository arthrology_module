<?php 

// Special thanx to Duilio Lupini (Speck - info@elxisitalia.com) for
// the translation!

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define('_ISO','charset=UTF-8');

define('CX_ARTH_SRCH_ENTER', 'Inserire una parola chiave per la ricerca (lunghezza minima 3 caratteri). Lasciare vuoto per vedere tutti gli articoli nella rivista selezionata.');
define('CX_ARTH_SRCH_MAG', 'Rivista');
define('CX_ARTH_SRCH_KEYW', 'Parola Chiave');

?>
