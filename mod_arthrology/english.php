<?php 

// Prevent direct inclusion of this file
defined( '_VALID_MOS' ) or die( 'Direct Access to this location is not allowed.' );

define('_ISO','charset=UTF-8');

define('CX_ARTH_SRCH_ENTER', 'Enter one keyword for search (al least 3 cahracters long). Leave empty for all articles in selected magazine.');
define('CX_ARTH_SRCH_MAG', 'Magazine');
define('CX_ARTH_SRCH_KEYW', 'Keyword');

?>
